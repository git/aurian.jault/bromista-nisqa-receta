import { Injectable } from "@angular/core";
import { Ingredient } from "../model/ingredient.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  private path:string = "https://664ba07f35bbda10987d9f99.mockapi.io/api/ingredients";

  constructor(private http: HttpClient) { }

  getIngredient() : Observable<Ingredient[]>{
    return this.http.get<Ingredient[]>(this.path);
  }

  add(ingredient: Ingredient): Observable<Ingredient> {
    return this.http.post<Ingredient>(this.path, ingredient);
  }

  update(ingredient: Ingredient): Observable<Ingredient> {
    return this.http.put<Ingredient>(`${this.path}/${ingredient.id}`, ingredient);
  }
}
