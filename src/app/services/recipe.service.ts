import { Injectable } from '@angular/core';
import { Recipe } from '../model/recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private localStorageKey = 'recipes';
  private recipes: Recipe[] = [];

  constructor() { }

  getRecipeById(id: number) {
    return this.recipes.find(e => e.id === id);
  }

  // Get recipes from local storage
  getRecipes(): Recipe[] {
    const recipesJson = localStorage.getItem(this.localStorageKey) || "[]";
    this.recipes = JSON.parse(recipesJson) || [];
    return this.recipes;
  }

  // Add a new recipe
  addRecipe(recipe: Recipe): number {
    this.getRecipes();
    recipe.id = this.recipes.length
    let newId = this.recipes.push(recipe);
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.recipes));
    return newId;
  }

  // Clear all recipes (for example, if needed)
  clearRecipes(): void {
    localStorage.removeItem(this.localStorageKey);
  }
}
