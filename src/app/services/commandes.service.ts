import { Injectable } from "@angular/core";
import { Recipe } from "../model/recipe.model";
@Injectable({
  providedIn: 'root'
})
export class CommandeService {
  private commande: Recipe[] = [];
  private localStorageKey = 'commande';

  constructor(){
    console.log(this.commande);
  }

  getRecipeById(id: number) {
    return this.commande.find(e => e.id === id);
  }

  // Get recipes from local storage
  getRecipe(): Recipe[] {
    const recipesJson = localStorage.getItem(this.localStorageKey) || "[]";
    this.commande = JSON.parse(recipesJson) || [];
    return this.commande;
  }

  // Add a new recipe
  addRecipe(recipe: Recipe): number {
    this.getRecipe();
    recipe.id = this.commande.length
    let newId = this.commande.push(recipe);
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.commande));
    return newId;
  }

  // Clear all recipes (for example, if needed)
  clearRecipes(): void {
    localStorage.removeItem(this.localStorageKey);
  }
}
