export interface Ingredient{
  id: number;
  name: string;
  qty: number;
  description: string;
}
