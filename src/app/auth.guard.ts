import { inject } from "@angular/core";
import { Router } from "@angular/router";

export const AuthGuard = () => {
  const router = inject(Router);
  const rawCookie = decodeURIComponent(document.cookie);
  const array = rawCookie.split(";");
  const name = "isAdmin=";
  let res:String = "";

  for(let cookie of array)
  {
    while (cookie.charAt(0) === ' ') {
        cookie = cookie.substring(1);
    }
    if (cookie.indexOf(name) === 0) {
        res = cookie.substring(name.length, cookie.length);
    }
  }

  console.log(res);

   // Check cookie
  if(res !== "true") {
      router.navigateByUrl('/login')
      return false
  }
  return true
}
