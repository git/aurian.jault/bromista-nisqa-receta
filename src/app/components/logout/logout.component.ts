import { Component } from '@angular/core';
import { Inject } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  standalone: true,
  imports: [],
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {
  constructor(@Inject(Router) private router: Router) {}


  ngOnInit(){
    document.cookie = "isAdmin=false";
    this.router.navigateByUrl('/');
  }
}
