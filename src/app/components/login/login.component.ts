import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { Inject } from '@angular/core';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  constructor(@Inject(Router) private router: Router) {}


  setCookie():void{
    document.cookie = "isAdmin=true";
    this.router.navigateByUrl('/ingredients');
  }
}
