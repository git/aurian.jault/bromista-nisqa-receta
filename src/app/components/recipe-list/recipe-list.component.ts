import { Component } from '@angular/core';
import { Recipe } from '../../model/recipe.model';
import { RecipeService } from '../../services/recipe.service';
import { NgFor } from '@angular/common';
import { RecipeMiniComponent } from '../recipe-mini/recipe-mini.component';
import { TranslocoPipe } from '@jsverse/transloco';
import { MatPaginatorModule, PageEvent} from '@angular/material/paginator';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  standalone: true,
  imports: [NgFor, RecipeMiniComponent, TranslocoPipe, MatPaginatorModule, RouterLink],
  templateUrl: './recipe-list.component.html',
  styleUrl: './recipe-list.component.css'
})
export class RecipeListComponent {
  recipes: Recipe[] = [];
  paginatedRecipes: Recipe[] = [];
  length?: number;
  pageSize = 10;
  pageIndex = 0;

  constructor(protected recipeService: RecipeService) { }

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
    this.length = this.recipes.length;
    this.paginatedRecipes = this.recipes.slice(this.pageIndex * this.pageSize, (this.pageIndex +1) * this.pageSize)
  }

  handlePageEvent(e: PageEvent) {
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.paginatedRecipes = this.recipes.slice(this.pageIndex * this.pageSize, (this.pageIndex +1) * this.pageSize)
  }
}
