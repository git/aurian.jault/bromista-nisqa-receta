import { Component} from '@angular/core';
import { Recipe } from '../../model/recipe.model';
import { RecipeService } from '../../services/recipe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslocoPipe } from '@jsverse/transloco';
import { IngredientMiniComponent } from '../ingredient-mini/ingredient-mini.component';
import { NgFor } from '@angular/common';


@Component({
  selector: 'app-recipe-detail',
  standalone: true,
  imports: [NgFor, TranslocoPipe, IngredientMiniComponent],
  templateUrl: './recipe-detail.component.html',
  styleUrl: './recipe-detail.component.css'
})
export class RecipeDetailComponent {

  recipe!: Recipe;
  id: string | null = null;

  constructor(private router: Router, private route: ActivatedRoute, private recipeService: RecipeService) {
    this.recipeService.getRecipes();
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      if (this.id) {
        this.recipe = this.recipeService.getRecipeById(Number(this.id))!;
        console.log(this.recipe);
      }
      if (this.recipe === undefined) {
        this.router.navigate(['/error/404'])
      }
    });
  }

}
