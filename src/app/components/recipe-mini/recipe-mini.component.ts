import { Component } from '@angular/core';
import { Recipe } from '../../model/recipe.model';
import { Input } from '@angular/core';
import { TruncatePipe } from '../../pipes/truncate.pipe';
import { TranslocoPipe } from '@jsverse/transloco';
import { RouterModule } from '@angular/router';
import { CommandeService } from '../../services/commandes.service';

@Component({
  selector: 'app-recipe-mini',
  standalone: true,
  imports: [TruncatePipe, TranslocoPipe, RouterModule],
  templateUrl: './recipe-mini.component.html',
  styleUrl: './recipe-mini.component.css'
})
export class RecipeMiniComponent {
  @Input() recipe!: Recipe;

  constructor(protected serviceCommande: CommandeService){}

  addRecipe(recipe: Recipe)
  {
    this.serviceCommande.addRecipe(recipe);
  }
}
