import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeMiniComponent } from './recipe-mini.component';

describe('RecipeMiniComponent', () => {
  let component: RecipeMiniComponent;
  let fixture: ComponentFixture<RecipeMiniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RecipeMiniComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RecipeMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
