import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Recipe } from '../../model/recipe.model';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RecipeService } from '../../services/recipe.service';
import { TranslocoPipe } from '@jsverse/transloco';
import { Ingredient } from '../../model/ingredient.model';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule} from '@angular/material/select';
import { Router } from '@angular/router';
import { IngredientService } from '../../services/ingredient.service';

@Component({
  selector: 'app-recipe-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslocoPipe,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule
  ],
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.css']
})

export class RecipeFormComponent {
  recipeForm: FormGroup;
  base64Image: string | ArrayBuffer | null = null;
  ingredientsOptions! : Ingredient[];
  defaultOption: string = 'Veuillez choisir';
  filename: string = '';

  constructor(private fb: FormBuilder, private recipeService: RecipeService, private router: Router,private serviceIngredients: IngredientService) {
    this.recipeForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      image: ['', Validators.required],
      ingredients: this.fb.array([]),
    });
    this.serviceIngredients.getIngredient().subscribe(ingredients => {
      this.ingredientsOptions = ingredients;
    });
  }

  get ingredients(): FormArray {
    return this.recipeForm.get('ingredients') as FormArray;
  }

  addIngredient(): void {
    this.ingredients.push(this.fb.group({
      name: ['', Validators.required],
      qty: ['', Validators.required]
    }));
  }

  removeIngredient(index: number): void {
    this.ingredients.removeAt(index);
  }

  onFileChange(event: Event) {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files[0]) {
      const file = input.files[0];
      this.filename = file.name;
      const reader = new FileReader();
      reader.onload = () => {
        this.base64Image = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  onSubmit(): void {
    if (this.recipeForm.valid) {
      const newRecipe: Recipe = {
        id: 0,
        name: this.recipeForm.value.name,
        description: this.recipeForm.value.description,
        image: this.base64Image?.toString() || "no-data",
        ingredients: this.recipeForm.value.ingredients.map((ingredient: Ingredient, idx: number) => ({
          id: idx,
          name: ingredient.name,
          qty: ingredient.qty,
        }))
      };
      console.log('Recipe added:', newRecipe);
      let newId = this.recipeService.addRecipe(newRecipe);
      this.router.navigate(['recipe', newId - 1]);
    } else {
      console.log('Form is invalid');
    }
  }
}

