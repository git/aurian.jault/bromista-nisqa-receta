import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientMiniComponent } from './ingredient-mini.component';

describe('IngredientMiniComponent', () => {
  let component: IngredientMiniComponent;
  let fixture: ComponentFixture<IngredientMiniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IngredientMiniComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IngredientMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
