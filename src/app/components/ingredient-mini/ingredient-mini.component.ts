import { Component } from '@angular/core';
import { Ingredient } from '../../model/ingredient.model'
import { Input } from '@angular/core'; 

@Component({
  selector: 'app-ingredient-mini',
  standalone: true,
  imports: [],
  templateUrl: './ingredient-mini.component.html',
  styleUrl: './ingredient-mini.component.css'
})
export class IngredientMiniComponent {
  @Input() ingredient!: Ingredient;
}
