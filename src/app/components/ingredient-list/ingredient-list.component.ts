import { Component } from '@angular/core';
import { Ingredient } from '../../model/ingredient.model';
import { IngredientService } from '../../services/ingredient.service';
import { OnInit } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-ingredient-list',
  standalone: true,
  imports: [NgIf, NgFor, FormsModule],
  providers: [],
  templateUrl: './ingredient-list.component.html',
  styleUrl: './ingredient-list.component.css'
})
export class IngredientListComponent {
  public ingredients! : Ingredient[];
  newIngredient: Ingredient = { id: 0, name: '', description: '', qty: 0 };
  editIngredient: Ingredient | null = null;

  constructor(private serviceIngredient : IngredientService){}

  ngOnInit(): void
  {
    this.serviceIngredient.getIngredient().subscribe(ingredients => {
      this.ingredients = ingredients;
    });
  }

  edit(ingre: Ingredient){
    this.editIngredient = ingre;
  }

  addIngredient() {
    this.serviceIngredient.add(this.newIngredient).subscribe(ingredient => {
      this.ingredients.push(ingredient);
      this.newIngredient = { id: 0, name: '', description: '', qty: 0}
    });
  }

  updateIngredient() {
    if (this.editIngredient) {
      this.serviceIngredient.update(this.editIngredient).subscribe(updatedIngredient => {
        const index = this.ingredients.findIndex(i => i.id === updatedIngredient.id);
        this.ingredients[index] = updatedIngredient;
        this.editIngredient = null;
      });
    }
  }

  cancelEdit() {
    this.editIngredient = null;
  }
}
