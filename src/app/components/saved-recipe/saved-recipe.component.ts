import { Component } from '@angular/core';
import { CommandeService } from '../../services/commandes.service';
import { OnInit } from '@angular/core';
import { Recipe } from '../../model/recipe.model';
import { NgFor, NgIf } from '@angular/common';
import { RecipeMiniComponent } from '../recipe-mini/recipe-mini.component';


@Component({
  selector: 'app-saved-recipe',
  standalone: true,
  imports: [NgIf, NgFor, RecipeMiniComponent],
  templateUrl: './saved-recipe.component.html',
  styleUrl: './saved-recipe.component.css'
})
export class SavedRecipeComponent {
  public commandes?: Recipe[];

  constructor(private serviceCommande : CommandeService){}

  ngOnInit(){
    this.commandes = this.serviceCommande.getRecipe();
  }

  deleteCart(){
    this.commandes = [];
    this.serviceCommande.clearRecipes();
  }
}
