import { Component } from '@angular/core';
import { RouterOutlet, RouterLink } from '@angular/router';
import { RecipeFormComponent } from './components/recipe-form/recipe-form.component';
import { RecipeService } from './services/recipe.service';
import { Recipe } from './model/recipe.model';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import { TranslocoPipe, TranslocoService } from '@jsverse/transloco';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterLink,
    RecipeFormComponent,
    RecipeListComponent,
    TranslocoPipe,
    NgIf,
  ],
  providers: [RecipeService, TranslocoService, NgIf],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'bromista-nisqa-receta';
  isLogged = false;
  constructor(protected recipeService: RecipeService, private translocoService: TranslocoService){}

  ngOnInit(){
    const rawCookie = decodeURIComponent(document.cookie);
    const array = rawCookie.split(";");
    const name = "isAdmin=";
    let res:String = "";

    for(let cookie of array)
    {
      while (cookie.charAt(0) === ' ') {
          cookie = cookie.substring(1);
      }
      if (cookie.indexOf(name) === 0) {
          res = cookie.substring(name.length, cookie.length);
      }
    }
    if (res !== "true" ){
      this.isLogged = false
    }else
    {
      this.isLogged = true;
    }
  }
  addRecipe($event: Recipe): void {
    this.recipeService.addRecipe($event);
  }

  changeLanguage(event: Event) {
    if (event.target && event.target instanceof HTMLSelectElement) {
      const lang = event.target.value;
      this.translocoService.setActiveLang(lang);
    } else {
      console.log('Error on language option value')
    }
  }
}
