import { Routes } from '@angular/router'

import { Error404Component } from "./components/errors/errors.component";
import { RecipeFormComponent } from "./components/recipe-form/recipe-form.component";
import { RecipeListComponent } from "./components/recipe-list/recipe-list.component";
import { RecipeDetailComponent } from './components/recipe-detail/recipe-detail.component';
import { LoginComponent } from './components/login/login.component';
import { IngredientListComponent } from './components/ingredient-list/ingredient-list.component';
import { LogoutComponent } from './components/logout/logout.component';
import { AuthGuard } from './auth.guard';
import { SavedRecipeComponent } from './components/saved-recipe/saved-recipe.component';

export const routes: Routes = [
  { path: '', component: RecipeListComponent },
  { path: 'error/:status', component: Error404Component},

  { path: 'recipe/add', component: RecipeFormComponent},
  { path: 'cart', component: SavedRecipeComponent},
  { path: 'recipe/:id', component: RecipeDetailComponent},
  { path: 'ingredients', component: IngredientListComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]  },
];
